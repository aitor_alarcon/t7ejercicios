import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class T7Ejercicio4App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
				
		// Creo un Hashtable que almacenar� el nombre y el precio del producto
		Hashtable<String, Double> stockTienda = new Hashtable<String, Double>();
		
		stockTienda.put("Lejia", 2.0);
		stockTienda.put("Detergente", 2.5);
		stockTienda.put("Colonia", 8.4);
		stockTienda.put("Desodorante", 1.99);
		stockTienda.put("Leche", 1.2);
		stockTienda.put("Agua", 0.9);
		stockTienda.put("Cereales", 1.0);
		stockTienda.put("Pollo", 4.1);
		stockTienda.put("Pizza", 2.0);
		stockTienda.put("Doritos", 1.5);

		double totalProductos = 0;
		
		// Creo el contador para la cantidad de productos que pasar� por caja
		int numProductos = 1;
		
		// Creo un String vac�o para poder controlarlo con el DoWhile
		String respuesta = "";
	
		System.out.println("Bienvenido al programa");
		
		// Creo el Do While
		do {
			
			System.out.println("Como se llama el producto n�mero " + numProductos + "?");
			String nombreProducto = teclado.next();
			// Le pregunto cuanto cuesta el producto que va a pasar primero?
			System.out.println("Cuanto cuesta el producto n�mero " + numProductos + "?");
			double precioProducto = teclado.nextDouble();
			
			stockTienda.remove(nombreProducto, precioProducto);
			
			// Voy sumando los precios de los productos para calcular el total
			totalProductos = totalProductos + precioProducto;
			
			// Incremento el contador de productos en 1
			numProductos++;
			
			// Pregunto si hay m�s productos
			System.out.println("Hay m�s productos? Si/No");
			respuesta = teclado.next();
			
			// Si la respuesta es si se vuelve a repetir el Do While
		} while (respuesta.equalsIgnoreCase("Si"));
		
		// Si la respuesta es no pregunto que IVA le va a aplicar a los productos
		System.out.println("Que IVA vas a aplicar a los productos? 21 o 4");
		int IVA = teclado.nextInt();
		
		// Muestro el IVA aplicado
		System.out.println("El IVA aplicado es = " + IVA + "%");

		// Muesto el total bruto de los productos
		System.out.println("Este es el total del precio bruto = " + totalProductos + "�");
		
		// Muestro el total de los productos m�s el IVA llamando a la funci�n para calcular el IVA
		System.out.println("Este es el total del precio m�s el IVA = " + calcularIVA(totalProductos, IVA) + "�");
		
		// Muestro el total de los productos comprados
		System.out.println("El n�mero de productos comprados es = " + (numProductos - 1));
		
		System.out.println("");
		
		// Pregunto al cajer@ cuanto va a pagar el cliente
		System.out.println("Cuanto va a pagar el cliente?");
		double pagoCliente = teclado.nextDouble();
		
		// Y muestro la cantidad a devolver al cliente llamando al calculo del cambioDinero m�s la de calcular el IVA
		System.out.println("El cambio a devolver al cliente es = " + cambioDinero(pagoCliente, calcularIVA(totalProductos, IVA)) + "�");
		
		System.out.println("Pago finalizado");
		
		System.out.println("Hay que a�adir algun producto a la base de datos? Si/No");
		String eleccion = teclado.next();
		
		// Creo un if para que si la respuesta es "Si" hace lo siguiente
		if(eleccion.equalsIgnoreCase("Si")) {
			
			// Creo un Do While para repetir el proceso de a�adir un producto
			// Si hace falta
			do {
				
				if(eleccion.equalsIgnoreCase("Si")) {
					
					// Le pregunto el nombre del pregunto
					System.out.println("Introduce el nombre del producto");
					String nombreProducto = teclado.next();
					
					// Le pregunto el precio del producto
					System.out.println("Introduce el precio del producto");
					double precioProducto = teclado.nextDouble();
					
					// Introduce el nombre y el precio del producto en la base de datos
					stockTienda.put(nombreProducto, precioProducto);
				}
				
				// Le pregunto si quiere a�adir otro producto
				System.out.println("Quieres a�adir otro producto? Si/No");
				eleccion = teclado.next();		
				
				// Sigue haciendo este proceso mientras la respuesta sea si
			} while (eleccion.equalsIgnoreCase("Si"));
		}
		
		// Le pregunto si quiere ver un articulo en especifico o todos
		System.out.println("Que quieres hacer? Ver un articulo o ver todos?");
		System.out.println("uno/todos");
		String eleccionVerProductos = teclado.next();
			
		// Creo un switch con la eleccion de como quiere ver los productos
		switch (eleccionVerProductos) {
		case "uno":
			// Le pido el nombre del producto
			System.out.println("Cu�l es el nombre del producto");
			String nombreProductoConsulta = teclado.next();
			
			// Muestro el precio del producto
			System.out.println(stockTienda.get(nombreProductoConsulta) + "�");
			break;

		case "todos":
			// Si es todos la eleccion le aviso de que voy a mostrar todos los productos
			System.out.println("Te voy a listar todos los productos en Stock:");
				
			// Creo dos enumerations para poder listar todos los productos
			Enumeration<String> nombreListaProductos = stockTienda.keys();
			Enumeration<Double> listaProductos = stockTienda.elements();
				
			// Creo un while para listar todos los productos mientras los haya
			while(nombreListaProductos.hasMoreElements()) {
				System.out.println("" + nombreListaProductos.nextElement() + " cuesta = " + listaProductos.nextElement() + "�");
			}
				
			break;
		default:
			// Por defecto digo que no ha querido ver ning�n producto
			System.out.println("No has querido ver ning�n producto");
			break;	
			
		}
	}
	
		// Creo el m�todo calcularIVA 
		public static double calcularIVA(double totalProductos, int IVA) {
			
			// Creo un double para guardar la cantidad de IVA de los productos
			double ivaProductos = 0;
			
			// Si el iva escogido es el 21
			if(IVA == 21) {
				// Guarda en ivaProductos el 21% de la cantidad bruta
				ivaProductos = totalProductos * 0.21;
			}
			// Si el iva escogido es el 4
			else {
				// Guarda en ivaProductos el 4% de la cantidad bruta
				ivaProductos = totalProductos * 0.4;
			}
			
			// Almaceno en totalProductos la cantidad bruta m�s el iva
			totalProductos = totalProductos + ivaProductos;
			
			// Retorno totalProductos
			return totalProductos;
		}
		
		// Creo la funci�n cambioDinero
		public static double cambioDinero(double pagoCliente, double precioIVA) {
			
			// Calculo en el totalCambio el pago del cliente menos el precio con IVA
			double totalCambio = pagoCliente - precioIVA;
			
			// retorno totalCambio
			return totalCambio;
		
		}
		
	

}
