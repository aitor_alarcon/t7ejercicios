import java.util.ArrayList;
import java.util.Scanner;

public class T7Ejercicio2App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Llamo al m�todo programaCaja
		programaCaja();
		
	}
	
	// Creo el m�todo programaCaja
	public static void programaCaja() {
		
		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Creo el arrayList del precio de los productos
		ArrayList<Double> listaPrecioProductos = new ArrayList<>();
		
		// Creo el contador para el total del precio los productos
		double totalProductos = 0;
		
		// Creo el contador para la cantidad de productos que pasar� por caja
		int numProductos = 1;
		
		// Creo un String vac�o para poder controlarlo con el DoWhile
		String respuesta = "";
	
		System.out.println("Bienvenido al programa");
		
		// Creo el Do While
		do {
			
			// Le pregunto cuanto cuesta el producto que va a pasar primero?
			System.out.println("Cuanto cuesta el producto n�mero " + numProductos + "?");
			double precioProducto = teclado.nextDouble();
			
			// A�ado el precio al arrayList
			listaPrecioProductos.add(precioProducto);
			
			// Voy sumando los precios de los productos para calcular el total
			totalProductos = totalProductos + precioProducto;
			
			// Incremento el contador de productos en 1
			numProductos++;
			
			// Pregunto si hay m�s productos
			System.out.println("Hay m�s productos? Si/No");
			respuesta = teclado.next();
			
			// Si la respuesta es si se vuelve a repetir el Do While
		} while (respuesta.equalsIgnoreCase("Si"));
		
		// Si la respuesta es no pregunto que IVA le va a aplicar a los productos
		System.out.println("Que IVA vas a aplicar a los productos? 21 o 4");
		int IVA = teclado.nextInt();
		
		// Muestro el IVA aplicado
		System.out.println("El IVA aplicado es = " + IVA + "%");

		// Muesto el total bruto de los productos
		System.out.println("Este es el total del precio bruto = " + totalProductos + "�");
		
		// Muestro el total de los productos m�s el IVA llamando a la funci�n para calcular el IVA
		System.out.println("Este es el total del precio m�s el IVA = " + calcularIVA(totalProductos, IVA) + "�");
		
		// Muestro el total de los productos comprados
		System.out.println("El n�mero de productos comprados es = " + (numProductos - 1));
		
		System.out.println("");
		
		// Pregunto al cajer@ cuanto va a pagar el cliente
		System.out.println("Cuanto va a pagar el cliente?");
		double pagoCliente = teclado.nextDouble();
		
		// Y muestro la cantidad a devolver al cliente llamando al calculo del cambioDinero m�s la de calcular el IVA
		System.out.println("El cambio a devolver al cliente es = " + cambioDinero(pagoCliente, calcularIVA(totalProductos, IVA)) + "�");
		
			
			
		}
		
	// Creo el m�todo calcularIVA 
	public static double calcularIVA(double totalProductos, int IVA) {
		
		// Creo un double para guardar la cantidad de IVA de los productos
		double ivaProductos = 0;
		
		// Si el iva escogido es el 21
		if(IVA == 21) {
			// Guarda en ivaProductos el 21% de la cantidad bruta
			ivaProductos = totalProductos * 0.21;
		}
		// Si el iva escogido es el 4
		else {
			// Guarda en ivaProductos el 4% de la cantidad bruta
			ivaProductos = totalProductos * 0.4;
		}
		
		// Almaceno en totalProductos la cantidad bruta m�s el iva
		totalProductos = totalProductos + ivaProductos;
		
		// Retorno totalProductos
		return totalProductos;
	}
	
	// Creo la funci�n cambioDinero
	public static double cambioDinero(double pagoCliente, double precioIVA) {
		
		// Calculo en el totalCambio el pago del cliente menos el precio con IVA
		double totalCambio = pagoCliente - precioIVA;
		
		// retorno totalCambio
		return totalCambio;
	}

}
