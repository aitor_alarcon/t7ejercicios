import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class T7Ejercicio3App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Creo un Hashtable que almacenar� el nombre y el precio del producto
		Hashtable<String, Double> stockTienda = new Hashtable<String, Double>();
		
		// Introduzco 10 productos en la base de datos
		stockTienda.put("Lejia", 2.0);
		stockTienda.put("Detergente", 2.5);
		stockTienda.put("Colonia", 8.4);
		stockTienda.put("Desodorante", 1.99);
		stockTienda.put("Leche", 1.2);
		stockTienda.put("Agua", 0.9);
		stockTienda.put("Cereales", 1.0);
		stockTienda.put("Pollo", 4.1);
		stockTienda.put("Pizza", 2.0);
		stockTienda.put("Doritos", 1.5);
		
		// Le pregunto al usuario si quiere a�adir un producto en la base de datos
		System.out.println("Quieres a�adir un producto en la base de datos? Si/No");
		String eleccion = teclado.next();
		
		// Creo un if para que si la respuesta es "Si" hace lo siguiente
		if(eleccion.equalsIgnoreCase("Si")) {
			
			// Creo un Do While para repetir el proceso de a�adir un producto
			// Si hace falta
			do {
				
				if(eleccion.equalsIgnoreCase("Si")) {
					
					// Le pregunto el nombre del pregunto
					System.out.println("Introduce el nombre del producto");
					String nombreProducto = teclado.next();
					
					// Le pregunto el precio del producto
					System.out.println("Introduce el precio del producto");
					double precioProducto = teclado.nextDouble();
					
					// Introduce el nombre y el precio del producto en la base de datos
					stockTienda.put(nombreProducto, precioProducto);
				}
				
				// Le pregunto si quiere a�adir otro producto
				System.out.println("Quieres a�adir otro producto? Si/No");
				eleccion = teclado.next();		
				
				// Sigue haciendo este proceso mientras la respuesta sea si
			} while (eleccion.equalsIgnoreCase("Si"));
		}
		
		// Le pregunto si quiere ver un articulo en especifico o todos
		System.out.println("Que quieres hacer? Ver un articulo o ver todos?");
		System.out.println("uno/todos");
		String eleccionVerProductos = teclado.next();
			
		// Creo un switch con la eleccion de como quiere ver los productos
		switch (eleccionVerProductos) {
		case "uno":
			// Le pido el nombre del producto
			System.out.println("Cu�l es el nombre del producto");
			String nombreProductoConsulta = teclado.next();
			
			// Muestro el precio del producto
			System.out.println(stockTienda.get(nombreProductoConsulta) + "�");
			break;

		case "todos":
			// Si es todos la eleccion le aviso de que voy a mostrar todos los productos
			System.out.println("Te voy a listar todos los productos en Stock:");
				
			// Creo dos enumerations para poder listar todos los productos
			Enumeration<String> nombreListaProductos = stockTienda.keys();
			Enumeration<Double> listaProductos = stockTienda.elements();
				
			// Creo un while para listar todos los productos mientras los haya
			while(nombreListaProductos.hasMoreElements()) {
				System.out.println("" + nombreListaProductos.nextElement() + " cuesta = " + listaProductos.nextElement() + "�");
			}
				
			break;
		default:
			// Por defecto digo que no ha querido ver ning�n producto
			System.out.println("No has querido ver ning�n producto");
			break;
		
		}
	}

}
