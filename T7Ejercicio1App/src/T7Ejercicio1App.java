import java.util.Hashtable;
import java.util.Scanner;

public class T7Ejercicio1App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Declaro un hash donde guardar� las notas medias de los alumnos
		Hashtable<String, Double> notaMedia = new Hashtable<String, Double>();
		
		// Declaro un hash donde guardar� el alumno y todas sus notas
		Hashtable<String, double[]> alumnos = new Hashtable<String, double[]>();
		
		// Creo un String de elecci�n para usarlo de condicionante en el do while
		String eleccion = "";
		
		// Creo un Do While
		do {
			
			// Le pido al usuario que introduzca el nombre del alumno
			System.out.println("Introduce el alumno");
			String alumno = teclado.next();
			
			// Le pido la cantidad de notas que va a introducir
			System.out.println("Cuantas notas vas a querer introducir?");
			int cantNotas = teclado.nextInt();
			
			// Creo un double donde almacenar� la media de notas del alumno
			double mediaAlumno = 0;
			
			// Creo un double donde guardar� la suma de todas las notas del alumno
			// para luego calcular la media
			double sumaNotasAlumno = 0;
			
			// Creo un vector donde almacenar� las notas del alumno en cuesti�n
			double notas[] = new double[cantNotas];
			
			// Creo un for para rellenar el vector con las notas
			for (int i = 0; i < cantNotas; i++) {
				
				// Le pido que introduzca la nota
				System.out.println("Introduce la nota " + (i+1));
				double nota = teclado.nextDouble();
				
				// Guardo la nota en el vector
				notas[i] = nota;
				
				// Sumo todas las notas introducidas en el vector
				sumaNotasAlumno = sumaNotasAlumno + nota;
				
			}
			
			// Calculo la media de las notas
			mediaAlumno = sumaNotasAlumno/cantNotas;
			
			// A�ado el alumno y todas sus notas en el hash alumnos
			alumnos.put(alumno, notas);
			
			// A�ado el alumno y la media de sus notas en el hash notaMedia
			notaMedia.put(alumno, mediaAlumno);
			
			// Muestro la nota media del alumno en cuesti�n
			System.out.println("La nota media de " + alumno + " es = " + notaMedia.get(alumno));
			
			// Le pregunto si quiere a�adir otro alumno
			System.out.println("Quieres a�adir otro alumno? Si/No");
			eleccion = teclado.next();
			
			// Depende de la elecci�n se repetir� todo el proceso o no
		} while (!eleccion.equalsIgnoreCase("no"));
		
		
	}

}
